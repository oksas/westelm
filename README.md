# westelm

> Williams-Sonoma challenge

## Project Overview

An item display page, using sample data from west elm. The project is built with [Vue](https://vuejs.org/), using the Vue CLI (webpack-simple config), to quickly scaffold an app with a modern, sensible structure. To get a clear picture of what the CLI generates versus what work was done to get the project to this state, compare the first commit in the repository to the latest one.

As per the CLI setup, webpack is used with babel to allow for ES6 usage, and SCSS.

For *instructions on running the project*, see below in the [Build Setup](#build-setup) section.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Project Structure

The `/src/` folder in the project is broken up into the following:

* `components`: this is where each single-file Vue component lives
* `containers`: also for Vue components, but for components that would be treated as "pages" specifically. If a state-management library was being used, the connections to the store would happen in these files.
* `data`: houses the sample data.
* `styles`: holds the styles; `_app.scss` is for app-wide styles, while `_variables.scss` contains _only_ scss variables for the project that can be imported into each Vue component (if necessary). Styles specific to each piece of UI lives in the corresponding `__.Vue` file.
* `App.vue`: the root component of the application
* `main.js`: the place where the application is instantiated

If `npm run build` is run, the project is compiled into a bundle inside the `/dist/` folder.

## Major Features

* The project renders a series of tiles/cards, based on the provided JSON object, for a list of products. Each card renders the product hero image, product name, and price, with a link to the product description page. 
* Clicking on the hero image for each product brings up an image carousel for the selected product, allowing the user to view each image for the product. Hovering over the hero image prompts the user to click to view the carousel.
* Because the JSON file seems to lack the appropriate CORS headers for cross-domain usage, the JSON file has been downloaded and included locally in the repo. The loading of the JSON response is simulated by using a Promise with `setTimeout`.
* While the JSON data "loads" when the page starts, the user is shown a loading animation.
* The page layout goes from 1 column to 3 columns, depending on screen size.

## UI/UX

The project uses 100% original styling, with a simple, clean visual design. The page itself is not terribly complex, leaving not a lot of room for UI/UX decisions to be made necessarily, but nonetheless a few considerations have been made:

* In general, brief animations/transitions have been used where it makes sense. These allow the app to feel more polished by reducing the jarring effects of elements instantly popping in/out.
* The page features a loading graphic to let the user know that the page data is on the way.
* When hovering over a card's hero image, the user is told that they can click the hero to see more images. The "click hero image -> open carousel" pattern is often implied in situations like this, but it's good to make it explicit for the user what the result of their interactions will be.
* The information section of each card (which contains the product name, price range, and link to view more) is itself actually wrapped in an anchor tag, meaning that the user can click the entire section to go to the product page. While each card does have an explicit "view details" link, making the whole section clickable allows the user to think as little as possible about where they need to click to get where they want to go.
* When the user opens a carousel, they are shown (in the form of dots in the carousel controls) how many images they can view, and which image they are on, which is pretty common practice. In addition, the "previous" and "next" buttons are adjacent in the carousel controls, rather than on each end of the dot indicators, to minimize the distance the user needs to move their mouse/finger if they want to quickly perform next/previous actions. I prefer the aesthetic of having the arrows on each end of the dots, but from a UX standpoint that can be a frustrating pattern if there are too many images in the carousel that place the arrows too far apart from each other.
